from flask import Flask, request
from csv import DictReader, DictWriter, QUOTE_NONE
from os.path import exists, getsize
from flask.json import jsonify
from http import HTTPStatus

import ipdb

FILENAME = 'users.csv'
FIELDNAMES = ['id', 'name', 'email', 'password', 'age']

# FUNÇÕES AUXILIARES


def get_last_id():

    with open(FILENAME, 'r') as f:
        reader = DictReader(f)
        id_list = [int(user['id']) for user in reader]

    return sorted(id_list)[-1] + 1 if id_list != [] else 1


def get_csv_data(filename):
    with open(filename, "r") as f:
        return [user for user in DictReader(f)]


def populate_csv(filename, data):
    with open(filename, "w") as file:
        writer = DictWriter(file, fieldnames=FIELDNAMES)

        writer.writeheader()
        writer.writerows(data)

# APP


def create_app():
    app = Flask(__name__)

    @app.route('/signup', methods=['POST'])
    def signup():

        if not exists(FILENAME) or getsize(FILENAME) == 0:
            with open(FILENAME, 'w') as f:
                writer = DictWriter(f, fieldnames=FIELDNAMES)
                writer.writeheader()

        csv_user = get_csv_data(FILENAME)

        for user in csv_user:
            if user['email'] == dict(request.get_json()).get('email'):
                return {}, HTTPStatus.UNPROCESSABLE_ENTITY

        user = dict(request.get_json())
        user['id'] = get_last_id()

        with open(FILENAME, 'a', newline='') as f:
            writer = DictWriter(f, fieldnames=FIELDNAMES)
            writer.writerow(user)

        del user['password']

        return user, HTTPStatus.CREATED

    @app.route('/login', methods=['POST'])
    def login():

        csv_user = get_csv_data(FILENAME)
        email_request = dict(request.get_json()).get('email')
        password_request = dict(request.get_json()).get('password')

        for user in csv_user:
            if user['email'] == email_request and user['password'] == password_request:

                del user['password']

                return user, HTTPStatus.ACCEPTED

        return {}, HTTPStatus.BAD_REQUEST

    @app.route('/profile/<int:user_id>', methods=['PATCH'])
    def update_user(user_id):

        user_request = request.get_json()
        csv_user = get_csv_data(FILENAME)

        for user in csv_user:
            if int(user["id"]) == user_id:
                user.update(user_request)
                break

        populate_csv(FILENAME, csv_user)

        del user['password']

        return user, HTTPStatus.OK

    @app.route('/profile/<int:user_id>', methods=['DELETE'])
    def delete_user(user_id):
        csv_user = get_csv_data(FILENAME)

        for user in csv_user:
            if int(user['id']) == user_id:
                csv_user.remove(user)

        populate_csv(FILENAME, csv_user)

        return {}, HTTPStatus.NO_CONTENT

    @app.route('/users')
    def all_users():
        csv_user = get_csv_data(FILENAME)

        users = []

        for user in csv_user:
            del user['password']

            users.append(user)

        return jsonify(users), HTTPStatus.OK

    return app
